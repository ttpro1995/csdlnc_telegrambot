

### Requirement

* Java 8 
* Gradle 


### How to deploy

Example

```
206.189.84.191  is your VPS 

# build 
gradle stage 

# upload jar to vps (optional: does not require to run locally on PC)  
rsync -aurv build/libs/telegrambot-3.0-all.jar  tthien@206.189.84.191:/home/tthien/java-app/telegrambot

# run java app 
DEV_BOT_TOKEN=bot-token DEV_BOT_NAME=bot-name NEO4J_IP=your-neo4j-instance-ip NEO4J_BOLT_PORT=your-neo4j-port NEO4J_USERNAME=your-neo4j-username NEO4J_PASSWORD=your-neo4j-password java -jar /home/tthien/java-app/telegrambot/telegrambot-3.0-all.jar

### 
interactive with @BotFather to create new bot and get  bot-token 
More documentation on Telegram bot https://core.telegram.org/bots
```

