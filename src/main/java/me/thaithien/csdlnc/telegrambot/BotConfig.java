package me.thaithien.csdlnc.telegrambot;

public class BotConfig {
    public static final String DEV_BOT_TOKEN = System.getenv("DEV_BOT_TOKEN");
    public static final String DEV_BOT_NAME = System.getenv("DEV_BOT_NAME");

    public static final String NEO4J_IP = System.getenv("NEO4J_IP");
    public static final String NEO4J_BOLT_PORT = System.getenv("NEO4J_BOLT_PORT");
    public static final String NEO4J_USERNAME = System.getenv("NEO4J_USERNAME");
    public static final String NEO4J_PASSWORD = System.getenv("NEO4J_PASSWORD");

}
