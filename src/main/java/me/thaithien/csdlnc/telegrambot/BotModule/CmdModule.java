package me.thaithien.csdlnc.telegrambot.BotModule;

import me.thaithien.csdlnc.telegrambot.Model.Product;
import me.thaithien.csdlnc.telegrambot.constant.CMD;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

import java.util.List;
import java.util.StringJoiner;


/**
 * return result to bot base on command
 */
public class CmdModule {

    /**
     * Parse command
     * @param msg  Message from telegram
     * @return
     */
    public static SendMessage process(Message msg){
        String msgStr = msg.getText();
        String resultStr = processStr(msgStr);
        SendMessage sendMessage = new SendMessage(msg.getChatId(), resultStr);
        return sendMessage;
    }

    /**
     * process the command
     * @param msgStr
     * @return
     */
    public static String processStr(String msgStr){
        if (msgStr.startsWith(CMD.CMD_QUERY_PRODUCT)){
            String inStr = msgStr.substring(CMD.CMD_QUERY_PRODUCT.length()).trim();
            String resultStr = queryProduct(inStr);
            return resultStr;
        } else if (msgStr.startsWith(CMD.CMD_HELP)){
            return CMD.HELP;
        } else  if (msgStr.startsWith(CMD.CMD_QUERY_CATEGORY)){
            String inStr = msgStr.substring(CMD.CMD_QUERY_CATEGORY.length()).trim();
            String resultStr = queryCategory(inStr);
            return resultStr;
        } else if (msgStr.startsWith(CMD.CMD_QUERY_PRICE_MAX)){
            String inStr = msgStr.substring(CMD.CMD_QUERY_PRICE_MAX.length()).trim();
            String resultStr = queryPriceMinMax(inStr, false);
            return resultStr;
        } else if (msgStr.startsWith(CMD.CMD_QUERY_PRICE_MIN)){
            String inStr = msgStr.substring(CMD.CMD_QUERY_PRICE_MIN.length()).trim();
            String resultStr = queryPriceMinMax(inStr, true);
            return resultStr;
        } else if (msgStr.startsWith(CMD.CMD_QUERY_PRICE_RANGE)){
            String inStr = msgStr.substring(CMD.CMD_QUERY_PRICE_RANGE.length()).trim();
            String[] inStrs = inStr.split(" ");
            String resultStr = queryPriceRange(inStrs[0], inStrs[1]);
            return resultStr;
        } else if (msgStr.startsWith(CMD.CMD_QUERY_PRODUCT_MAX)){
            String inStr = msgStr.substring(CMD.CMD_QUERY_PRODUCT_MAX.length()).trim();
            String resultStr = queryPriceRank(inStr, false);
            return resultStr;
        } else if (msgStr.startsWith(CMD.CMD_SAME_CATEGORY)){
            String inStr = msgStr.substring(CMD.CMD_SAME_CATEGORY.length()).trim();
            String resultStr = querySameCategory(inStr);
            return resultStr;
        } else if (msgStr.startsWith(CMD.CMD_LIST_CATEGORY)){
            return listCategory();
        } else if (msgStr.startsWith(CMD.CMD_QUERY_PRODUCT_MIN)){
            String inStr = msgStr.substring(CMD.CMD_QUERY_PRODUCT_MIN.length()).trim();
            String resultStr = queryPriceRank(inStr, true);
            return resultStr;
        }
        return null;
    }


    public static String queryProduct(String inStr){
        if (inStr.length()==0){
            return "Không tìm thấy sản phẩm";
        }

        QueryModule module = QueryModule.getInstace();
        try {
            List<Product> products = module.queryProduct(inStr);
            StringJoiner joiner = new StringJoiner("\n");
            if (products.size() == 0){
                return "Không tìm thấy sản phẩm";
            }
            for (Product p : products){

                if (products.size() < 15){
                    joiner.add(p.toStringLong());
                } else {
                    joiner.add(p.toString());
                }
            }
            return joiner.toString();
        } catch (Exception e){
            return "Có lỗi xảy ra ";
        }
    }

    public static String queryCategory(String inStr){
        if (inStr.length()==0){
            return "Không tìm thấy sản phẩm";
        }

        QueryModule module = QueryModule.getInstace();
        try {
            List<Product> products = module.queryCategory(inStr);
            StringJoiner joiner = new StringJoiner("\n");
            if (products.size() == 0){
                return "Không tìm thấy sản phẩm";
            }
            for (Product p : products){
                joiner.add(p.toString());
            }
            return joiner.toString();
        } catch (Exception e){
            return "Có lỗi xảy ra ";
        }
    }

    public static String queryPriceMinMax(String priceStr, boolean isMin){
        int price = Integer.parseInt(priceStr);
        QueryModule module = QueryModule.getInstace();
        try {
            List<Product> products = module.queryPrice(price, isMin);
            StringJoiner joiner = new StringJoiner("\n");
            if (products.size() == 0){
                System.out.println();
                return "Không tìm thấy sản phẩm";
            }
            for (Product p : products){
                if (products.size() < 15){
                    joiner.add(p.toStringLong());
                } else {
                    joiner.add(p.toString());
                }
            }
            return joiner.toString();
        } catch (Exception e){
            return "Có lỗi xảy ra ";
        }
    }

    public static String queryPriceRange(String priceMinStr, String priceMaxStr){
        int priceMin = Integer.parseInt(priceMinStr);
        int priceMax = Integer.parseInt(priceMaxStr);
        QueryModule module = QueryModule.getInstace();
        try {
            List<Product> products = module.queryPrice(priceMin, priceMax);
            StringJoiner joiner = new StringJoiner("\n");
            if (products.size() == 0){
                return "Không tìm thấy sản phẩm";
            }
            for (Product p : products){
                if (products.size() < 15){
                    joiner.add(p.toStringLong());
                } else {
                    joiner.add(p.toString());
                }
            }
            return joiner.toString();
        } catch (Exception e){
            e.printStackTrace();
            return "Có lỗi xảy ra ";
        }
    }

    public static String queryPriceRank(String product, boolean isMin){

        QueryModule module = QueryModule.getInstace();
        List<Product> productList = null;
        if (product == null || product.equals("")){
            try {
                productList = module.queryPriceRank(isMin);
                if (productList.size() == 0){
                    return "Không tìm thấy sản phẩm";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Có lỗi";
            }
        } else {
            try {
                productList = module.queryPriceRank(product, isMin);
                if (productList.size() == 0){
                    return "Không tìm thấy sản phẩm";
                }
            } catch (Exception e) {
                e.printStackTrace();
                return "Có lỗi";
            }
        }
        StringJoiner joiner = new StringJoiner("\n");

        for (Product p : productList){
            if (productList.size() < 15){
                joiner.add(p.toStringLong());
            } else {
                joiner.add(p.toString());
            }
        }
        return joiner.toString();
    }




    public static String querySameCategory(String itemCode){
        QueryModule module = QueryModule.getInstace();
        List<Product> productList = null;
        try {
            productList = module.querySameCategory(itemCode);
        } catch (Exception e) {
            e.printStackTrace();
            return "Có lỗi";
        }
        StringJoiner joiner = new StringJoiner("\n");

        for (Product p : productList){
            if (productList.size() < 15){
                joiner.add(p.toStringLong());
            } else {
                joiner.add(p.toString());
            }
        }
        return joiner.toString();
    }

    public static String listCategory(){
        QueryModule module = QueryModule.getInstace();
        try {
            return module.queryListCategory();
        } catch (Exception e) {
            e.printStackTrace();
            return "Có lỗi";
        }
    }
}
