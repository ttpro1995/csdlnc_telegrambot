package me.thaithien.csdlnc.telegrambot.BotModule;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

public class EchoModule {

    /**
     * Just echo the message
     * @param msg
     * @return
     */
    public static SendMessage echo(Message msg){
        SendMessage sendMessage = new SendMessage(msg.getChatId(),"echo : "+ msg.getText());
        return sendMessage;
    }
}
