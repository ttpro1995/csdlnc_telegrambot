package me.thaithien.csdlnc.telegrambot.BotModule;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

/**
 * always query for product
 */
public class NaiveModule {
    public static SendMessage process(Message msg){
        String msgStr = msg.getText();
        String resultStr = processStr(msgStr);
        SendMessage sendMessage = new SendMessage(msg.getChatId(), resultStr);
        return sendMessage;
    }

    public static String processStr(String msgStr){
        String resultStr = CmdModule.queryProduct(msgStr);
        return resultStr;
    }
}
