package me.thaithien.csdlnc.telegrambot.BotModule;


import me.thaithien.csdlnc.telegrambot.BotConfig;
import me.thaithien.csdlnc.telegrambot.Model.Item;
import me.thaithien.csdlnc.telegrambot.Model.Product;
import me.thaithien.csdlnc.telegrambot.util.NlpTool;
import me.thaithien.simpleneo4j.Neo4jManager;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;

import java.util.*;

/**
 * Query for thing
 */
public class QueryModule {
    public static QueryModule INSTANCE = null;

    private static List<Product> query(String cypher) throws Exception {
        Map<String, Product> productMap = new LinkedHashMap<>();
        Neo4jManager.query(cypher).forEach(mapVal->{
            String qProductName = mapVal.get("p.name");
            String url = mapVal.get("p.url");
            Product product;
            if (!productMap.containsKey(qProductName)){
                product = new Product(qProductName, url);
                productMap.put(qProductName, product);
            } else {
                product = productMap.get(qProductName);
            }
            if (mapVal.get("i.code")!=null) {
                String itemCode = mapVal.get("i.code");
                String itemPrice = mapVal.get("i.price");

                Item item = new Item(itemCode, itemPrice);
                product.addItem(item);
            }
        });
        List<Product> result = new ArrayList<>(productMap.values());
        return result;
    }

    public static QueryModule getInstace(){
        if (INSTANCE == null){
            INSTANCE = new QueryModule();
            // Neo4jManager.initialDriver("188.166.233.199", "7687", "tthien", "neo4jmeow");
            Neo4jManager.initialDriver(BotConfig.NEO4J_IP, BotConfig.NEO4J_BOLT_PORT, BotConfig.NEO4J_USERNAME, BotConfig.NEO4J_PASSWORD);
        }
        return INSTANCE;
    }

    private QueryModule(){

    }

    public List<Product> queryProduct(String productName) throws Exception {
        String queryTemplate = "MATCH (p:PRODUCT)-[:HAS]-(i:ITEM) WHERE p.name=~ '(?i).*zProductNamez.*' OR p.normalized_name=~ '.*(?i)zNorProductz.*' return p, i LIMIT 15";
        String cypher = queryTemplate.replace("zProductNamez", productName);
        cypher = cypher.replace("zNorProductz", NlpTool.normalized_name(productName));
        System.out.println(cypher);
        List<Product> result = query(cypher);
        return result;
    }

    /**
     * Query for category and product belong to category
     * @param category
     * @return
     * @throws Exception
     */
    public List<Product> queryCategory(String category) throws Exception {
        String queryTemplate = "// query category \n" +
                "MATCH (c:CATEGORY)-[:BELONG_TO]-(p:PRODUCT)\n" +
                "WHERE c.name=~'.*(?i)zCategoryz.*' OR c.normalized_name=~'.*(?i)zNorCategoryz.*'  \n" +
                "return c, p\n" +
                "LIMIT 25";
        String cypher = queryTemplate;
        cypher = cypher.replace("zCategoryz", category);
        cypher = cypher.replace("zNorCategoryz", NlpTool.normalized_name(category));
        System.out.println(cypher);
        Map<String, Product> productMap = new HashMap<>();
        Neo4jManager.query(cypher).forEach(mapVal->{
            String qProductName = mapVal.get("p.name");
            String url = mapVal.get("p.url");
            Product product;
            if (!productMap.containsKey(qProductName)){
                product = new Product(qProductName, url);
                productMap.put(qProductName, product);
            }
        });
        List<Product> result = new ArrayList<>(productMap.values());
        return result;
    }

    public List<Product> queryPrice(int price, boolean isMin) throws Exception {

        String queryTemplate = "MATCH (p:PRODUCT)-[:HAS]-(i:ITEM)\n" +
                "WHERE i.price zComparatorz zPricez \n" +
                "RETURN p, i \n" +
                "ORDER BY i.price DESC \n" +
                "LIMIT 15";
        String cypher = queryTemplate;
        cypher = cypher.replace("zPricez", Integer.toString(price));
        String comparator = "";
        if (isMin){
            comparator = "<=";
        } else {
            comparator = ">=";
            cypher = cypher.replace("DESC", "");
        }
        cypher = cypher.replace("zComparatorz", comparator);
        System.out.println(cypher);
        List<Product> result = query(cypher);
        return result;
    }

    public List<Product> queryPrice(int priceMin, int priceMax) throws Exception {
        String queryTemplate = "// price range  \n" +
                "MATCH (p:PRODUCT)-[:HAS]-(i:ITEM)  \n" +
                "WHERE i.price >= zPriceMinz AND i.price <= zPriceMaxz \n" +
                "RETURN p, i \n" +
                "ORDER BY i.price  \n" +
                "LIMIT 15";
        String cypher = queryTemplate;
        cypher = cypher.replace("zPriceMinz", Integer.toString(priceMin));
        cypher = cypher.replace("zPriceMaxz", Integer.toString(priceMax));
        System.out.println(cypher);
        List<Product> result = query(cypher);
        return result;
    }

    public String queryListCategory() throws Exception {
        String cypher = "MATCH (c:CATEGORY) return c";
        Set<String> categorySet = new HashSet<>();
        Neo4jManager.query(cypher).forEach(mapVal -> {
            String category = mapVal.get("c.name");
            categorySet.add(category);
        });
        StringJoiner joiner = new StringJoiner("\n");
        joiner.add("Danh sách các chủng loại sản phẩm");
        for (String category : categorySet){
            joiner.add(category);
        }
        System.out.println(cypher);
        return joiner.toString();
    }

    /**
     * Query product and rank the price
     * @param pName product name
     * @param isMin true is cheapest
     * @return
     */
    public List<Product> queryPriceRank(String pName, boolean isMin) throws Exception {
        String cypherTemplate = "MATCH (p:PRODUCT)-[:HAS]-(i:ITEM)\n" +
                "WHERE p.name=~ '.*(?i)zProductz.*' OR p.normalized_name=~ '.*(?i)zNorProductz.*' \n" +
                "return p, i \n" +
                "ORDER BY i.price DESC \n" +
                "LIMIT 10" ;
        String cypher = cypherTemplate;
        String normalizedName = NlpTool.normalized_name(pName);
        cypher = cypher.replace("zProductz", pName);
        cypher = cypher.replace("zNorProductz", normalizedName);
        if (isMin){
            cypher = cypher.replace("DESC", "");
        }
        System.out.println(cypher);
        List<Product> result = query(cypher);
        return result;
    }

    public List<Product> queryPriceRank(boolean isMin) throws Exception {
        String cypherTemplate = "\n" +
                "MATCH (p:PRODUCT)-[:HAS]-(i:ITEM)\n" +
                "return p, i \n" +
                "ORDER BY i.price DESC \n" +
                "LIMIT 10";
        String cypher = cypherTemplate;
        if (isMin){
            cypher = cypher.replace("DESC", "");
        }
        System.out.println(cypher);
        List<Product> result = query(cypher);
        return result;
    }

    public List<Product> querySameCategory(String itemCode) throws Exception {
        String cypherTemplate = "MATCH (i:ITEM)-[:HAS]-(pp:PRODUCT)-[:BELONG_TO]-(c:CATEGORY), (c)-[:BELONG_TO]-(p:PRODUCT)  " +
                "WHERE i.code = 'zCodez'" +
                "RETURN p " +
                "LIMIT 25";
        String cypher = cypherTemplate;
        cypher = cypher.replace("zCodez", itemCode);
        System.out.println(cypher);
        List<Product> result = query(cypher);
        return result;
    }




}
