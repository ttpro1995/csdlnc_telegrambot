package me.thaithien.csdlnc.telegrambot.BotUpdateHandler;

import me.thaithien.csdlnc.telegrambot.BotConfig;
import me.thaithien.csdlnc.telegrambot.BotModule.CmdModule;
import me.thaithien.csdlnc.telegrambot.BotModule.EchoModule;
import me.thaithien.csdlnc.telegrambot.BotModule.NaiveModule;
import org.apache.commons.lang3.time.StopWatch;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class DevBotUpdate extends TelegramLongPollingBot {
    @Override
    public void onUpdateReceived(Update update) {
    }


    @Override
    public void onUpdatesReceived(List<Update> updates) {
        for (Update update : updates){
            if (update.hasMessage()) {
                StopWatch timer = new StopWatch();
                timer.start();
                Message msg = update.getMessage();

                // do sthg here
                SendMessage sntMsg = msgBroker(msg);
                timer.stop();
                System.out.println(String.valueOf(timer.getTime(TimeUnit.MILLISECONDS)));

                try { // execute
                    execute(sntMsg);

                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public String getBotUsername() {
        return BotConfig.DEV_BOT_NAME;
    }

    @Override
    public String getBotToken() {
        return BotConfig.DEV_BOT_TOKEN;
    }

    public static SendMessage echoHandle(Message msg){
        System.out.println(msg);
        Long chatId = msg.getChatId();
        String text = msg.getText();
        System.out.println(text);
        SendMessage echoMsg = new SendMessage(chatId, text);
        return echoMsg;
    }


    public static SendMessage msgBroker(Message msg){
        if (msg.hasText()) {
            String text = msg.getText();
            if (text.startsWith("/")){
                return CmdModule.process(msg);
            }
            else return NaiveModule.process(msg);
            // return EchoModule.echo(msg);
        }
        return null;
    }
}
