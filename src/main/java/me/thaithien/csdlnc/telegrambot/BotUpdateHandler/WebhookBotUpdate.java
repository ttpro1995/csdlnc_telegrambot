package me.thaithien.csdlnc.telegrambot.BotUpdateHandler;

import me.thaithien.csdlnc.telegrambot.BotConfig;
import org.telegram.telegrambots.api.methods.BotApiMethod;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramWebhookBot;

public class WebhookBotUpdate extends TelegramWebhookBot {
    @Override
    public BotApiMethod onWebhookUpdateReceived(Update update) {
        return null;
    }

    @Override
    public String getBotUsername() {
        return BotConfig.DEV_BOT_NAME;
    }

    @Override
    public String getBotToken() {
        return BotConfig.DEV_BOT_TOKEN;
    }

    @Override
    public String getBotPath() {
        return null;
    }
}
