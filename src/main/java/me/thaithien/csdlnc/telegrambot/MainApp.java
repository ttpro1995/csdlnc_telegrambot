package me.thaithien.csdlnc.telegrambot;

import me.thaithien.csdlnc.telegrambot.BotModule.QueryModule;
import me.thaithien.csdlnc.telegrambot.BotUpdateHandler.DevBotUpdate;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

public class MainApp {
    public static void main(String[] args) {
        ApiContextInitializer.init();

        // init neo4j connection
        System.out.println("Starting QueryModule");
        QueryModule INSTANCE = QueryModule.getInstace();

        // starting telegram api
        TelegramBotsApi botsApi = new TelegramBotsApi();
        try {
            System.out.println("Starting telegram bot API");
            botsApi.registerBot(new DevBotUpdate());
            System.out.println("bot is working");
        } catch (TelegramApiException e) {
            e.printStackTrace();
            System.err.println("bot error");
            System.exit(0);
        }
    }
}
