package me.thaithien.csdlnc.telegrambot.Model;

public class Item {
    public String code;
    public String price;
    public String url;
    public int priceInt;

    public Item() {
    }

    public Item(String code, String price) {
        this.code = code;
        this.price = price;
        this.priceInt = Integer.parseInt(price);
    }

    @Override
    public String toString() {
        return "Mã: " + this.code + " Giá: " + this.price;
    }
}
