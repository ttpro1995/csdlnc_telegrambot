package me.thaithien.csdlnc.telegrambot.Model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

public class Product {
    public String productName;
    public String url;
    public Map<String, Item> hasItems;
    public int cheapestPrice;
    public int mostExpensivePrice;

    public Product() {
        hasItems = new HashMap<>();
    }

    public Product(String productName, String url) {
        this.hasItems = new HashMap<>();
        this.url = url;
        this.productName = productName;
    }

    /**
     *
     * @param aItem
     * @return
     */
    public boolean addItem(Item aItem){
        String code = aItem.code;
        if (this.hasItems.containsKey(code)){
            return false; // item exist
        }
        this.hasItems.put(code, aItem);
        if (aItem.priceInt > this.mostExpensivePrice){
            this.mostExpensivePrice = aItem.priceInt;
        }
        if (aItem.priceInt < this.cheapestPrice){
            this.cheapestPrice = aItem.priceInt;
        }

        return true;
    }

    @Override
    public String toString() {
        return this.productName + " " + this.url;
    }

    public String toStringLong(){
        StringJoiner joiner = new StringJoiner(" \n");
        joiner.add(this.productName + " " + this.url);
        for (Item item : this.hasItems.values()){
            joiner.add(item.toString());
        }
        return joiner.toString();
    }
}
