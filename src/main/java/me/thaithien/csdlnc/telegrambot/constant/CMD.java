package me.thaithien.csdlnc.telegrambot.constant;

public class CMD {
    public static String CMD_HELP = "/help";
    public static String CMD_QUERY_PRODUCT = "/tim_san_pham";
    public static String CMD_QUERY_CATEGORY = "/tim_loai";
    public static String CMD_QUERY_PRICE_MIN = "/tim_re_hon";
    public static String CMD_QUERY_PRICE_MAX = "/tim_mac_hon";
    public static String CMD_QUERY_PRICE_RANGE = "/tim_khoang_gia";
    public static String CMD_QUERY_PRODUCT_MAX = "/mac_nhat";
    public static String CMD_QUERY_PRODUCT_MIN = "/re_nhat";
    public static String CMD_SAME_CATEGORY = "/cung_loai";
    public static String CMD_LIST_CATEGORY = "/liet_ke_loai";

    public static String HELP = "Có thể gõ trực tiếp tên sản phẩm vào để tìm kiếm. \n" +
            "Hoặc dùng một trong các lệnh dưới đây \n" +
            "/tim_san_pham <tên> : tìm sản phẩm theo tên \n" +
            "/tim_loai <loại> liệt kê các sản phẩm theo loại \n" +
            "/tim_re_hon <giá> tìm các sản phẩm rẻ hơn mức giá  \n " +
            "/tim_mac_hon <giá> tìm sản phẩm mắc hơn mức giá  \n" +
            "/tim_khoang_gia <min> <max>  tìm sản phẩm trong khoảng giá \n " +
            "/cung_loai <code san pham>  tìm sản phẩm cùng loại \n" +
            "/re_nhat <optional: tên sản phẩm> tìm sản phẩm rẻ nhất \n" +
            "/mac_nhat <optional: tên sản phẩm> tìm sản phẩm mắc nhất \n" +
            "/liet_ke_loai  : Liệt kê tất cả các loại sản phẩm "  ;
}
