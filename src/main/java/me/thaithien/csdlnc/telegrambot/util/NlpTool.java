package me.thaithien.csdlnc.telegrambot.util;

import org.apache.commons.lang3.StringUtils;

public class NlpTool {
    public static String normalized_name(String inStr){
        return StringUtils.stripAccents(inStr).toLowerCase();
    }
}
