package me.thaithien.csdlnc.telegrambot.BotModule;

import org.junit.Test;

import static org.junit.Assert.*;

public class CmdModuleTest {

    @Test
    public void processStr() {

    }

    @Test
    public void queryProduct() {
        String inStr = "/tim_san_pham quần";
        String result = CmdModule.processStr(inStr);
        System.out.println(result);
        String inStr2 = "/tim_san_pham quần jean";
        String result2 = CmdModule.processStr(inStr2);
        System.out.println(result2);
    }

    @Test
    public void queryCategory() {
        String inStr = "/tim_loai đầm";
        String result = CmdModule.processStr(inStr);
        System.out.println(result);
    }

    @Test
    public void queryPriceMinMax() {
        String inStr = "/tim_re_hon 100000";
        String result = CmdModule.processStr(inStr);
        System.out.println(result);
        String inStr2 = "/tim_mac_hon 200000";
        String result2 = CmdModule.processStr(inStr2);
        System.out.println(result2);
    }

    @Test
    public void queryPriceRange() {
        String inStr = "/tim_khoang_gia 100000 150000";
        String result = CmdModule.processStr(inStr);
        System.out.println(result);
    }



    @Test
    public void queryPriceRank() {
        runQueryForOutput("/mac_nhat ");
        System.out.println("==========================");
        runQueryForOutput("/re_nhat ");
        System.out.println("==========================");
        runQueryForOutput("/mac_nhat váy ");
        System.out.println("==========================");
        runQueryForOutput("/re_nhat váy ");
    }

    @Test
    public void querySameCategory() {
        runQueryForOutput("/cung_loai donggia68");
    }

    @Test
    public void listCategory() {
        runQueryForOutput("/liet_ke_loai");
    }

    private void runQueryForOutput(String cypher){
        String inStr = cypher;
        String result = CmdModule.processStr(inStr);
        System.out.println(result);
    }

    private void runQueryNoOutput(String cypher){
        String inStr = cypher;
        String result = CmdModule.processStr(inStr);
    }


    @Test
    public void multipleCmd(){
        String[] cmdList = {"/cung_loai 353daf ",
                "/liet_ke_loai ",
                "/mac_nhat",
                "/mac_nhat váy ",
                "/re_nhat",
                "/re_nhat_váy",
                "/tim_khoang_gia 60000 80000",
                "/tim_loai bộ ",
                "/tim_loai đồ bộ",
                "/tim_mac_hon 200000",
                "/tim_re_hon 70000",
                "/tim_san_pham áo khoác nỉ",
                "/tim_san_pham quần jean",
                "/tim_san_pham set áo "};

        for (String cmd: cmdList){
            System.out.println("+=====================++++++");
            System.out.println(cmd);
            runQueryNoOutput(cmd);
        }
    }

}