package me.thaithien.csdlnc.telegrambot.BotModule;

import me.thaithien.csdlnc.telegrambot.Model.Item;
import me.thaithien.csdlnc.telegrambot.Model.Product;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class QueryModuleTest {

    @Test
    public void queryProduct() throws Exception {
        QueryModule instance = QueryModule.getInstace();
        List<Product> productList = instance.queryProduct("váy");
        for (Product p: productList){
            System.out.println(p.productName + " " + p.url);
        }
        System.out.println("===========");
        for (Product p: productList){
            System.out.println(p.productName + " " + p.url);
            for (Item item : p.hasItems.values()){
                System.out.println(item.code + " " + item.price);
            }
        }
    }
}